package com.adamkorzeniak.utils.exceptions.model.exception;

public abstract class BusinessException extends RuntimeException {

    public BusinessException(String message) {
        super(message);
    }

    public abstract String getCode();

    public abstract String getTitle();
}
